--[[
    I'M Orianna 0.4 by Klokje
    ========================================================================
 
    Info
    
    Change log: 
    0.4.1 - Small bugfix

    0.4:
        - Added Smartball
        - Added Auto Shield
        - Added Shield Initiation spells: Malphite, Akali, Hecarium, Jarvan, Jax, Katarina, Lee Sin, Amumu, Maokai, Irelia, Shyvana, Pantheon, Xin Zhao, Wukong 
        - Optimalisize Combo system 

    0.3.1 - 0.3.2:
        Fixed the shielding bug - tried casting the whole map.


    0.3:
        - Will still combos if Q and W are less then 1.5 s of cooldown. instead of no combo
        - Added "Disable R in combo", It will only do R if enough people(slider) in range of the ball
        - Will shield if ally is close enough to enough enemy in range of that ally(R slider)
        - Fixed some small bugs
        - Added new Combo. If ball is between ally and enemy. And enemy is in R range. It will cast R(ball will behind target). Do W and then E(damage) back to ally.

   
]]
 
-- Load required libraries -----------------------------------------------------
require "2DGeometry"

-- Globals ---------------------------------------------------------------------
if myHero.charName ~= "Orianna" then return end

local SortList = {
    ["Ashe"]= 1,["Caitlyn"] = 1,["Corki"] = 1,["Draven"] = 1,["Ezreal"] = 1,["Graves"] = 1,["Jayce"] = 1,["KogMaw"] = 1,["MissFortune"] = 1,["Quinn"] = 1,["Sivir"] = 1,
    ["Tristana"] = 1,["Twitch"] = 1,["Varus"] = 1,["Vayne"] = 1,

    ["Ahri"] = 2,["Annie"] = 2,["Akali"] = 2,["Anivia"] = 2,["Brand"] = 2,["Cassiopeia"] = 2,["Diana"] = 2,["Evelynn"] = 2,["FiddleSticks"] = 2,["Fizz"] = 2,["Gragas"] = 2,
    ["Heimerdinger"] = 2,["Karthus"] = 2,["Kassadin"] = 2,["Katarina"] = 2,["Kayle"] = 2,["Kennen"] = 2,["Leblanc"] = 2,["Lissandra"] = 2,["Lux"] = 2,["Malzahar"] = 2,["Zed"] = 2,
    ["Mordekaiser"] = 2,["Morgana"] = 2,["Nidalee"] = 2,["Orianna"] = 2,["Rumble"] = 2,["Ryze"] = 2,["Sion"] = 2,["Swain"] = 2,["Syndra"] = 2,["Teemo"] = 2,["TwistedFate"] = 2,
    ["Veigar"] = 2,["Viktor"] = 2,["Vladimir"] = 2,["Xerath"] = 2,["Ziggs"] = 2,["Zyra"] = 2,["MasterYi"] = 2,["Shaco"] = 2,["Jayce"] = 2,["Pantheon"] = 2,["Urgot"] = 2,["Talon"] = 2,
    
    ["Alistar"] = 3,["Blitzcrank"] = 3,["Janna"] = 3,["Karma"] = 3,["Leona"] = 3,["Lulu"] = 3,["Nami"] = 3,["Nunu"] = 3,["Sona"] = 3,["Soraka"] = 3,["Taric"] = 3,["Thresh"] = 3,["Zilean"] = 3,

    ["Darius"] = 4,["Elise"] = 4,["Fiora"] = 4,["Gangplank"] = 4,["Garen"] = 4,["Irelia"] = 4,["JarvanIV"] = 4,["Jax"] = 4,["Khazix"] = 4,["LeeSin"] = 4,["Nautilus"] = 4,
    ["Olaf"] = 4,["Poppy"] = 4,["Renekton"] = 4,["Rengar"] = 4,["Riven"] = 4,["Shyvana"] = 4,["Trundle"] = 4,["Tryndamere"] = 4,["Udyr"] = 4,["Vi"] = 4,["MonkeyKing"] = 4,
    ["Aatrox"] = 4,["Nocturne"] = 4,["XinZhao"] = 4,

    ["Amumu"] = 5,["Chogath"] = 5,["DrMundo"] = 5,["Galio"] = 5,["Hecarim"] = 5,["Malphite"] = 5,["Maokai"] = 5,["Nasus"] = 5,["Rammus"] = 5,["Sejuani"] = 5,["Shen"] = 5,
    ["Singed"] = 5,["Skarner"] = 5,["Volibear"] = 5,["Warwick"] = 5,["Yorick"] = 5,["Zac"] = 5
} 


local Cooldown = {
    [_Q] = 0,
    [_W] = 0,
    [_E] = 0,
    [_R] = 0,
}


local Initiation = {
    ["UFSlash"] = 0,
    ["HecarimUlt"] = 0,
    ["AkaliShadowDance"] = 0,
    ["JarvanIVCataclysm"] = 0,
    ["JaxLeapStrike"] = 0,
    ["KatarinaE"] = 0,
    ["BlindMonkWOne"] = 0,
    ["BlindMonkQTwo"] = 0,
    ["MaokaiUnstableGrowth"] = 0,
    ["IreliaGatotsu"] = 0,
    ["ShyvanaTransformCast"] = 0,
    ["Pantheon_LeapBash"] = 0,
    ["XenZhaoSweep"] = 0,
    ["MonkeyKingNimbus"] = 0,

    ["BandageToss"] = 1,
}

local bMoving = false
local ball = myHero

local qRange = 820
local wRange = 240
local eRange = 1120
local rRange = 350
local ballRange = 1325
local ballColor = 0x33FF00

local ignite = nil

local liandry = 3151
local blackfire = 3188

local DFGSlot = 3128
local HXGSlot = 3146
local BWCSlot = 3144

local selected = nil

local block = true



local attacktime = 0
local startAttackSpeed = ((0.95)*1.6)
local attackdelay = 0.3*(1-0.124561404)
local attacking = false
local tempPacket = nil

enemyHeroes = {}
allyHeroes = {}
local enemyMinions = {}


-- Code ------------------------------------------------------------------------


 function OnLoad()
    PrintChat(" >> I'M Orianna")

    ConfigBasic = scriptConfig("I'M Orianna: Basic Settings", "OriannaBasic") 
    ConfigBasic:addParam("Farm", "Farm", SCRIPT_PARAM_ONKEYTOGGLE, true, 90) 
    ConfigBasic:addParam("FarmpMana", "Farm: min % mana", SCRIPT_PARAM_SLICE, 20, 0, 100, 0)
    ConfigBasic:addParam("UseQ", "Farm: Use Q", SCRIPT_PARAM_ONOFF, true)
    ConfigBasic:addParam("UseW", "Farm: Use W", SCRIPT_PARAM_ONOFF, true)
    ConfigBasic:addParam("RCombo", "R in combo", SCRIPT_PARAM_ONOFF, true)
    ConfigBasic:addParam("SCombo", "Smart Ball", SCRIPT_PARAM_ONOFF, true)
    ConfigBasic:addParam("Harass", "Harass", SCRIPT_PARAM_ONKEYDOWN, false, 84) 
    ConfigBasic:addParam("Combo", "Combo", SCRIPT_PARAM_ONKEYDOWN, false, 32)
    
    ConfigBasic:permaShow("Farm") 
    ConfigBasic:permaShow("Harass") 
    ConfigBasic:permaShow("Combo") 

    ConfigShield = scriptConfig("I'M Orianna: Shield Settings", "OriannaSHield")
    
    for i=1, heroManager.iCount do
        local teammate = heroManager:GetHero(i)
        if teammate.team == myHero.team then 
            local max
            if not teammate.isMe then max = 50 - (SortList[teammate.charName]*5) else max = 100 end
            ConfigShield:addParam(teammate.charName, "Shield "..teammate.charName, SCRIPT_PARAM_ONOFF, true) 
            ConfigShield:addParam(teammate.charName .. "max", teammate.charName ..": max % health", SCRIPT_PARAM_SLICE, max, 0, 100, 0)
            ConfigShield:addParam(teammate.charName .. "CC", teammate.charName ..": Shield CC", SCRIPT_PARAM_ONOFF, teammate.isMe)
            ConfigShield:addParam(teammate.charName .. "SS", teammate.charName ..": Shield Skillshot", SCRIPT_PARAM_ONOFF, true)
        end
    end
    ConfigShield:addParam("harassMana", "", SCRIPT_PARAM_SLICE, 0, 0, 0, 0)
    ConfigShield:addParam("shieldult", "Combo Shield enemy", SCRIPT_PARAM_ONOFF, true)
    ConfigShield:addParam("shieldultenemy", "Combo: min enemys", SCRIPT_PARAM_SLICE, 3, 0, 5, 0)
    ConfigShield:addParam("shielddmg", "Shield: Damage enemy", SCRIPT_PARAM_ONOFF, true)


    Config = scriptConfig("I'M Orianna: Auto Settings", "Orianna") 
    Config:addParam("AutoStop", "Disable Auto function", SCRIPT_PARAM_ONKEYTOGGLE, false, 86) 
    Config:addParam("Autohealt", "Auto: min % health", SCRIPT_PARAM_SLICE, 35, 0, 100, 0)
    Config:addParam("AutoQ", "Auto Q", SCRIPT_PARAM_ONOFF, true)
    Config:addParam("AutoQpMana", "Auto Q: min % mana", SCRIPT_PARAM_SLICE, 30, 0, 100, 0)
    Config:addParam("AutoW", "Auto W", SCRIPT_PARAM_ONOFF, true)
    Config:addParam("AutoWpMana", "Auto W: min % mana", SCRIPT_PARAM_SLICE, 70, 0, 100, 0)
    Config:addParam("AutoWenemy", "Auto W: min Enemys", SCRIPT_PARAM_SLICE, 1, 0, 5, 0)
    Config:addParam("AutoR", "Auto R", SCRIPT_PARAM_ONOFF, true)
    Config:addParam("AutoRenemy", "Auto R: min enemys", SCRIPT_PARAM_SLICE, 3, 0, 5, 0)
    Config:addParam("RKs", "Auto: Smart Combo's", SCRIPT_PARAM_ONOFF, true)

    Config:permaShow("AutoStop") 


    ConfigDraw = scriptConfig("I'M Orianna: Draw Settings", "OriannaDraw")      
    ConfigDraw:addParam("DrawQ", "Draw Q range", SCRIPT_PARAM_ONOFF, true)
    ConfigDraw:addParam("DrawW", "Draw W range", SCRIPT_PARAM_ONOFF, true)
    ConfigDraw:addParam("DrawR", "Draw R range", SCRIPT_PARAM_ONOFF, true)
    ConfigDraw:addParam("DrawBall", "Draw Ball range", SCRIPT_PARAM_ONOFF, true)
    ConfigDraw:addParam("DrawTarget", "Draw Target", SCRIPT_PARAM_ONOFF, true)

    enemyMinions = minionManager(MINION_ENEMY, qRange + ballRange, player, MINION_SORT_HEALTH_ASC)

    for i = 1, objManager.maxObjects, 1 do
        local obj = objManager:GetObject(i)
        CheckBall(obj)
        CheckRangeColor(obj)
    end

    for i = 1, heroManager.iCount do
        local hero = heroManager:GetHero(i)
        if hero.team ~= myHero.team then
            table.insert(enemyHeroes, hero)
        else 
            table.insert(allyHeroes, hero)
        end
    end

    if myHero:GetSpellData(SUMMONER_1).name:find("SummonerDot") then ignite = SUMMONER_1
    elseif myHero:GetSpellData(SUMMONER_2).name:find("SummonerDot") then ignite = SUMMONER_2
    end
end

function OnSendPacket(p)
    if attacking and (p.header == 0x71 or p.header == 0x9A) then
        tempPacket =  copyPacket(p)
        p.pos = 1
        p:Block()
    end
end

function copyPacket(packet)
      packet.pos = 1
      p = CLoLPacket(packet.header)
      for i=1,packet.size-1,1 do
        p:Encode1(packet:Decode1())
      end
      p.dwArg1 = packet.dwArg1
      p.dwArg2 = packet.dwArg2
      return p
end

function OnProcessSpell(object,spell)
    if object~= nil and spell ~= nil and object.isMe then
        if spell.name:find("Attack") then
            attacking = true
            attacktime = GetGameTimer()
        elseif spell.name == "OrianaIzunaCommand" then 
            Cooldown[_Q] = GetGameTimer() + (myHero:GetSpellData(_Q).cd * (1 + myHero.cdr))
        elseif spell.name == "OrianaDissonanceCommand" then 
            Cooldown[_W] = GetGameTimer() + (myHero:GetSpellData(_W).cd * (1 + myHero.cdr))
        elseif spell.name == "OrianaRedactCommand" then 
            Cooldown[_E] = GetGameTimer() + (myHero:GetSpellData(_E).cd * (1 + myHero.cdr))
        elseif spell.name == "OrianaDetonateCommand" then 
            Cooldown[_R] = GetGameTimer() + (myHero:GetSpellData(_R).cd * (1 + myHero.cdr))
        end
    end

    if Initiation[spell.name] == 0 then
        if object.team == myHero.team  and GetDistance(object) < eRange then
            local count = 0
            for i, target in pairs(enemyHeroes) do
                if ValidTarget(target, qRange+ballRange) and GetDistance(spell.endPos, target) < rRange then
                    count = count + 1
                    if count >= ConfigShield.shieldultenemy then
                        CastSpell(_E, object)
                        break
                   end
               end
            end
            
        end
    end

    if Initiation[spell.name] == 1 then
        if object.team == myHero.team  and GetDistance(object) < eRange then
            local count = 0
            local hit = false
            for i, target in pairs(enemyHeroes) do
                if ValidTarget(target, qRange+ballRange) and GetDistance(spell.endPos, target) < rRange then
                    count = count + 1
                   if GetDistance(spell.endPos, target) < 80 then 
                       hit = true
                    end

                   if count >= ConfigShield.shieldultenemy and hit then
                        CastSpell(_E, object)
                       break
                    end
                end
            end
            
        end
    end

    Shield(object, spell)
end

function Shield(object,spell) 
    if not myHero:CanUseSpell(_E) == READY or object.team == myHero.team then return end


    for i, ally in pairs(allyHeroes) do
        if objectValid(ally) and GetDistance(ally) < eRange and ConfigShield[ally.charName] and ally.maxHealth *(ConfigShield[ally.charName.."max"]/100) >= ally.health then
            if object.type == "obj_AI_Turret" and GetDistance(ally, spell.endPos) < 80 then 
                CastSpell(_E, ally)
            elseif object.type == "obj_AI_Hero" and GetDistance(ally, spell.endPos) < 80  then 
                spelltype = getSpellType(object, spell.name)
                if spelltype == "BAttack" or spelltype == "CAttack" and GetDistance(ally, spell.endPos) < 80  then
                    local onhitdmg = 0
                    local onhittdmg = 0
                    local InfinityEdge = 0
                    local adamage = object:CalcDamage(ally,object.totalDamage)
                    if GetInventoryHaveItem(3186,object) then onhitdmg = getDmg("KITAES",ally,object) end
                    if GetInventoryHaveItem(3114,object) then onhitdmg = onhitdmg+getDmg("MALADY",ally,object) end
                    if GetInventoryHaveItem(3091,object) then onhitdmg = onhitdmg+getDmg("WITSEND",ally,object) end
                    if GetInventoryHaveItem(3057,object) then onhitdmg = onhitdmg+getDmg("SHEEN",ally,object) end
                    if GetInventoryHaveItem(3078,object) then onhitdmg = onhitdmg+getDmg("TRINITY",ally,object) end
                    if GetInventoryHaveItem(3100,object) then onhitdmg = onhitdmg+getDmg("LICHBANE",ally,object) end
                    if GetInventoryHaveItem(3025,object) then onhitdmg = onhitdmg+getDmg("ICEBORN",ally,object) end
                    if GetInventoryHaveItem(3087,object) then onhitdmg = onhitdmg+getDmg("STATIKK",ally,object) end
                    if GetInventoryHaveItem(3153,object) then onhitdmg = onhitdmg+getDmg("RUINEDKING",ally,object) end
                    if GetInventoryHaveItem(3042,object) then onhitdmg = onhitdmg+getDmg("MURAMANA",ally,object) end
                    if GetInventoryHaveItem(3209,object) then onhittdmg = getDmg("SPIRITLIZARD",ally,object) end
                    if GetInventoryHaveItem(3184,object) then onhittdmg = onhittdmg+80 end
                    if spelltype == "BAttack" then
                        onhitdmg = (adamage+onhitdmg)*1.07+onhittdmg
                    else
                        if GetInventoryHaveItem(3031,object) then InfinityEdge = .5 end
                        onhitdmg = (adamage*(2.1+InfinityEdge)+onhitdmg)*1.07+onhittdmg 
                    end
                    if onhitdmg >= ally.health or onhitdmg > ally.maxHealth *0.1 then 
                        CastSpell(_E, ally)
                    end
                elseif spelltype == "Q" or spelltype == "W" or spelltype == "E" or spelltype == "R" or spelltype == "P" or spelltype == "QM" or spelltype == "WM" or spelltype == "EM" then
                    local onhitspelltdmg, onhitspelldmg, onhitspelldmg, muramanadmg, dmg = 0, 0, 0, 0, 0
                    local hitchampion = false
                    local CC,Muramana = false,false,false,false,false
                    local shottype,radius,maxdistance = 0,0,0

                    if GetInventoryHaveItem(3151,object) then onhitspelldmg = getDmg("LIANDRYS",ally,object) end
                    if GetInventoryHaveItem(3188,object) then onhitspelldmg = getDmg("BLACKFIRE",ally,object) end
                    if GetInventoryHaveItem(3209,object) then onhitspelltdmg = getDmg("SPIRITLIZARD",ally,object) end
                    if GetInventoryHaveItem(3042,object) then muramanadmg = getDmg("MURAMANA",ally,object) end
                    CC = skillShield[object.charName][spelltype]["CC"]
                    Muramana = skillShield[object.charName][spelltype]["Muramana"]
                    shottype = skillData[object.charName][spelltype]["type"]
                    radius = skillData[object.charName][spelltype]["radius"]
                    maxdistance = skillData[object.charName][spelltype]["maxdistance"]
                    muramanadmg = Muramana and muramanadmg or 0
                    skilldamage = getDmg(spelltype,ally,object,3,spellLevel)

                    if shottype == 0 then hitchampion = checkhitaoe(object, spell.endPos, 80, ally, 0)
                    elseif shottype == 1 then hitchampion = checkhitlinepass(object, spell.endPos, radius, maxdistance, ally, getHitBoxRadius(myHero)*2-10)
                    elseif shottype == 2 then hitchampion = checkhitlinepoint(object, spell.endPos, radius, ally, getHitBoxRadius(myHero)*2-10)
                    elseif shottype == 3 then hitchampion = checkhitaoe(object, spell.endPos, radius, ally, getHitBoxRadius(myHero)*2-10)
                    elseif shottype == 4 then hitchampion = checkhitcone(object, spell.endPos, radius, maxdistance, ally, getHitBoxRadius(myHero)*2-10)
                    elseif shottype == 5 then hitchampion = checkhitwall(object, spell.endPos, radius, maxdistance, ally, getHitBoxRadius(myHero)*2-10)
                    elseif shottype == 6 then hitchampion = checkhitlinepass(object, spell.endPos, radius, maxdistance, ally, getHitBoxRadius(myHero)*2-10) or checkhitlinepass(object, Vector(object)*2-P2, radius, maxdistance, ally, getHitBoxRadius(myHero)*2-10)
                    end
                    dmg = (skilldamage+onhitspelldmg+muramanadmg)*1.07+onhitspelltdmg

                    if ((dmg >= ally.health or dmg > ally.maxHealth *0.1) and shottype == 0)  then
                        CastSpell(_E, ally)
                    end
                    if (CC == 2 and ConfigShield[ally.charName .. "CC"]) then 
                        CastSpell(_E, ally)
                    end
                    if (ConfigShield[ally.charName .. "SS"] and hitchampion and shottype ~= 0)  then
                        CastSpell(_E, ally)
                    end
                end 
            elseif object.type == "obj_AI_Minion" and GetDistance(ally, spell.endPos) < 80 then 
                local dmg = object:CalcDamage(ally,object.totalDamage)
                if  dmg >= ally.health or dmg > ally.maxHealth *0.1 then
                    CastSpell(_E, ally)
                end
            elseif spell.name:find("SummonerDot") and GetDistance(ally, spell.endPos) < 80 then
                dmg = getDmg("IGNITE",ally,object)
                 if  dmg >= ally.health or dmg > ally.maxHealth *0.1 then
                    CastSpell(_E, ally)
                end
            end

        end
    end
end




function CheckBall(obj)
    if obj == nil or obj.name == nil then return end  
    
    if (obj.name:find("Oriana_Ghost_mis") or obj.name:find("Oriana_Ghost_mis_protect") ) then
        bMoving = true
        return
    end

    if obj.name:find("yomu_ring_green") then
        ball = obj
        return
    end

    if obj.name:find("Oriana_Ghost_bind") then
        for i, target in pairs(allyHeroes) do
            if GetDistance(target, obj) < 50 then
                ball = target
           end
        end
    end
end

function RemoveBall(obj)
    if obj == nil or obj.name == nil then return end  

    if (obj.name:find("Oriana_Ghost_mis") or obj.name:find("Oriana_Ghost_mis_protect")) then
        bMoving = false
        return
    end

    if obj.name:find("yomu_ring_green") or obj.name:find("Oriana_Ghost_bind") then
        ball = myHero
        ballColor = 0x006600
        return
    end
end

function CheckRangeColor(obj)
    if obj == nil or obj.name == nil then return end  

    if obj.name == "OrianaBallIndicatorNear.troy" then
        ballColor = 0x006600
    elseif obj.name == "OrianaBallIndicatorMedium.troy" then
        ballColor = 4294967040
    elseif obj.name == "OrianaBallIndicatorFar.troy" then
        ballColor = 0xFF0000
    end
end


function OnCreateObj(obj)
    if obj == nil or obj.name == nil then return end  
   -- PrintChat(tostring(obj.name))
    CheckBall(obj)
    CheckRangeColor(obj)

    if GetDistance(obj) <100 and obj.name == "OrianaBasicAttack_mis.troy" and attacking then
        attacking = false
        if tempPacket ~= nil then
            SendPacket(tempPacket)
            tempPacket = nil
        end
    end
end

function OnDeleteObj(obj)
    RemoveBall(obj)
end

function DrawTextWithBorder(textToDraw, textSize, x, y, textColor, backgroundColor)
    DrawText(textToDraw, textSize, x + 1, y, backgroundColor)
    DrawText(textToDraw, textSize, x - 1, y, backgroundColor)
    DrawText(textToDraw, textSize, x, y - 1, backgroundColor)
    DrawText(textToDraw, textSize, x, y + 1, backgroundColor)
    DrawText(textToDraw, textSize, x , y, textColor)
end

function OnDraw()
    if ConfigDraw.DrawBall and not bMoving and not myHero.dead then
        if ball.type == "obj_AI_Hero" then 
            if not ball.isMe then
            DrawCircle(ball.x, ball.y, ball.z, ballRange+125, ballColor)
            end
        else 
            DrawCircle(ball.x, ball.y, ball.z, ballRange, ballColor)
        end
    end

    if ConfigDraw.DrawQ and not myHero.dead and myHero:CanUseSpell(_Q) == READY then
        DrawCircle(player.x, player.y, player.z, qRange+55, 0x00FFFF)
    end

    if ConfigDraw.DrawW and not bMoving and not myHero.dead and myHero:CanUseSpell(_W) == READY then
        DrawCircle(ball.x, ball.y, ball.z, wRange, 0x0000CC)
    end

    if ConfigDraw.DrawR and not bMoving and not myHero.dead and myHero:CanUseSpell(_R) == READY then
        DrawCircle(ball.x, ball.y, ball.z, rRange, 0xb8ff5c)
    end

    if ConfigDraw.DrawTarget and not myHero.dead then
        if selected then
            DrawCircle(selected.x, selected.y, selected.z, getHitBoxRadius(selected), 0xFFFFFF)
        else 
            for i, target in pairs(enemyHeroes) do
                if ValidTarget(target, 1400) then
                    DrawCircle(target.x, target.y, target.z, getHitBoxRadius(target), 0xFFDB0A)
                    break
                end
            end
        end 
    end

    for i, target in pairs(enemyHeroes) do
        DrawCalculation(target)
    end
end


function DrawCalculation(target)
    if not ValidTarget(target, 1400) or myHero.dead then return end

    local qDmg = getDmg("Q", target, player)
    local wDmg = getDmg("W", target, player)
    local eDmg = getDmg("E", target, player)
    local rDmg = getDmg("R", target, player)
    local pDmg = getDmg("P", target, player)
    local aDmg = getDmg("AD",target, player)
    local igniteDmg = (ignite and getDmg("IGNITE", target, player) or 0)
    local dfgDmg = (GetInventorySlotItem(DFGSlot) and getDmg("DFG", target, player) or 0)
    local hxgDmg = (GetInventorySlotItem(HXGSlot) and getDmg("HXG", target, player) or 0)
    local bwcDmg = (GetInventorySlotItem(BWCSlot) and getDmg("BWC", target, player) or 0)
    local extradmg = igniteDmg + dfgDmg + hxgDmg + bwcDmg

    local onspellDmg = (GetInventorySlotItem(liandry) and getDmg("LIANDRYS", target, player) or 0) + (GetInventorySlotItem(blackfire) and getDmg("BLACKFIRE", target, player) or 0)
    local onspellDmg = onspellDmg * 0.5
    local onhitDmg = pDmg + aDmg

    local mDmg = 0
    local mana = 0
    local maxDamage = qDmg + wDmg + rDmg + onspellDmg + onhitDmg + extradmg
    if maxDamage < target.health then return end

    if myHero:CanUseSpell(_Q) == READY and mana <= myHero.mana then   
        mDmg = mDmg + qDmg
        mDmg = mDmg + onspellDmg
        mana = mana + myHero:GetSpellData(_Q).mana
    end
    if myHero:CanUseSpell(_W) == READY and mana <= myHero.mana then   
        mDmg = mDmg + wDmg
        mDmg = mDmg + onspellDmg
        mana = mana + myHero:GetSpellData(_W).mana
    end
    if myHero:CanUseSpell(_R) == READY and mana <= myHero.mana then   
        mDmg = mDmg + rDmg
        mDmg = mDmg + onspellDmg
        mana = mana + myHero:GetSpellData(_R).mana
    end
    if ignite ~= nil and myHero:CanUseSpell(ignite) == READY then   
        mDmg = mDmg + igniteDmg
    end
    if myHero:CanUseSpell(DFGSlot) == READY then   
        mDmg = mDmg + dfgDmg
    end
    if myHero:CanUseSpell(HXGSlot) == READY then   
        mDmg = mDmg + hxgDmg
    end
    if myHero:CanUseSpell(BWCSlot) == READY then   
        mDmg = mDmg + bwcDmg
    end
    mDmg = mDmg + onhitDmg


    local pos = WorldToScreen(D3DXVECTOR3(target.x, target.y, target.z))
    if mDmg >= target.health then
        DrawTextWithBorder("Kill it!", 20, pos.x- 15, pos.y -11, 4293787648, 4278190080)
    elseif maxDamage >= target.health then
        DrawTextWithBorder("Cooldown or need mana", 20, pos.x- 15, pos.y - 11, 4294967040, 4278190080)
    end
end

function OnTick()
    Sorting()

    if attacking and attacktime + attackdelay < GetGameTimer() then
        if tempPacket ~= nil then
            SendPacket(tempPacket)
            tempPacket = nil
        end
        attacking = false
    end

    local currentTarget = GetTarget()

    if currentTarget ~= nil and currentTarget.type == "obj_AI_Hero" and ValidTarget(currentTarget, 2000, true) then
        selected = currentTarget
    else 
        selected = nil
    end

    if not Combo() then 
        if not AutoFarm() and (not Config.AutoStop or ConfigBasic.Combo or ConfigBasic.Harass) then 
            AutoAll()
        end
    end

    AutoShield()
end


function AutoShield()
    if not ConfigShield.shieldult then return end 

    for i, ally in pairs(allyHeroes) do
        local count = 0
        if GetDistance(ally) < eRange then
            for i, target in pairs(enemyHeroes) do
                if TargetNear(target, rRange, ally) then 
                    count = count + 1
                    if count >= ConfigShield.shieldultenemy and myHero:CanUseSpell(_E) == READY and  GetDistance(ally) < eRange then 
                        CastSpell(_E, ally)
                    end 
                end
            end
        end
    end
end


function AutoFarm()
    if ConfigBasic.Farm and not ConfigBasic.Combo and not ConfigBasic.Harass and myHero.maxMana*(ConfigBasic.FarmpMana/100) < myHero.mana and (ConfigBasic.UseW or ConfigBasic.UseQ) then
        enemyMinions:update()

        local QPrediction = TargetPredictionVIP(900, 1200, 0.250, 80, ball)
        local tempballQ = nil
        local WPrediction = TargetPredictionVIP(wRange, math.huge, 0.3, nil, ball)
        local tempballW = nil
        
        for index, minion in pairs(enemyMinions.objects) do
            local wDmg = getDmg("W", minion, player)
            local qDmg = getDmg("Q", minion, player)

            if ConfigBasic.UseW and minion.health < wDmg and GetDistance(ball, minion) < wRange and myHero:CanUseSpell(_W) == READY then
                if tempballW == nil then
                    if ball.type == "obj_AI_Hero" then
                        local pos1, t1, vec1 = WPrediction:GetPrediction(ball)
                        tempballW = pos1 
                    else 
                        tempballW = ball
                    end
                end
                return CastW(minion, tempballW)
            elseif ConfigBasic.UseQ and minion.health < qDmg and GetDistance(minion) < qRange and myHero:CanUseSpell(_Q) == READY then
                if tempballQ == nil then
                    if ball.type == "obj_AI_Hero" then 
                        local pos1, t1, vec1 = QPrediction:GetPrediction(ball)
                        tempballQ = pos1
                    else 
                        tempballQ = ball
                    end
                end

                return CastQ(minion, tempballQ)
            end
            return false
        end
    end
    return false
end

function Combo()
    if ((Config.AutoStop or not Config.RKs) and not ConfigBasic.Combo) or bMoving then return false end

    local QPrediction = TargetPredictionVIP(900, 1200, 0.250, 80, ball)
    local WPrediction = TargetPredictionVIP(wRange, math.huge, 0.3, nil, ball)
    local RPrediction = TargetPredictionVIP(rRange, math.huge, 0.3, nil, ball)
    local tempballQ = nil
    local tempballW = nil
    local tempballR = nil

    for i, target in pairs(enemyHeroes) do
        if ValidTarget(target, qRange+ballRange) then
            local extradmg = 0
            local qDmg = getDmg("Q", target, player)
            local wDmg = getDmg("W", target, player)
            local eDmg = getDmg("E", target, player)
            local rDmg = getDmg("R", target, player)
            local pDmg = getDmg("P", target, player)
            local aDmg = getDmg("AD",target, player)
            if ignite ~= nil and myHero:CanUseSpell(ignite) == READY  and GetDistance(target) < 600 then extradmg = extradmg + getDmg("IGNITE", target, player) end
            if GetInventorySlotItem(DFGSlot) and myHero:CanUseSpell(DFGSlot) == READY  and GetDistance(target) < 750 then extradmg = extradmg + getDmg("DFG", target, player) end
            if GetInventorySlotItem(HXGSlot) and myHero:CanUseSpell(DFGSlot) == READY  and GetDistance(target) < 700 then extradmg = extradmg + getDmg("HXG", target, player) end
            if GetInventorySlotItem(BWCSlot) and myHero:CanUseSpell(DFGSlot) == READY  and GetDistance(target) < 500 then extradmg = extradmg + getDmg("BWC", target, player) end

            local onspellDmg = (GetInventorySlotItem(liandry) and getDmg("LIANDRYS", target, player) or 0) + (GetInventorySlotItem(blackfire) and getDmg("BLACKFIRE", target, player) or 0)
            local onspellDmg = onspellDmg * 0.5
            local onhitDmg = pDmg + aDmg


            qDmg = qDmg + onspellDmg
            wDmg = wDmg + onspellDmg
            rDmg = rDmg + onspellDmg

             if tempballQ == nil then
                if ball.type == "obj_AI_Hero" then 
                    local pos1, t1, vec1 = QPrediction:GetPrediction(ball)
                    tempballQ = pos1
                else 
                    tempballQ = ball
                end
            end
            if tempballW == nil then
                if ball.type == "obj_AI_Hero" then
                    local pos1, t1, vec1 = WPrediction:GetPrediction(ball)
                    tempballW = pos1 
                else 
                    tempballW = ball
                end
            end
            if tempballR == nil then
                if ball.type == "obj_AI_Hero" then
                    local pos1, t1, vec1 = RPrediction:GetPrediction(ball)
                    tempballR = pos1 
                else 
                    tempballR = ball
                end
            end
            


            if (qDmg + eDmg + wDmg + rDmg + extradmg + onhitDmg) < target.health then break end

            if target.health < onhitDmg and GetDistance(target) < 500 then 
                myHero:Attack(target)
            end

            if target.health < extradmg then 
                CastIgnite(target)
                CastItems(target)
                return true
            end

            if target.health < extradmg + onhitDmg and GetDistance(target) < 500 then 
                myHero:Attack(target)
                CastIgnite(target)
                CastItems(target)
                return true
            end

             if target.health < (wDmg + extradmg) and CastW(target, tempballW) then
                CastIgnite(target)
                CastItems(target)
                return true
            end

             if target.health < (wDmg + extradmg + onhitDmg) and GetDistance(target) < 500 and CastW(target, tempballW) then
                myHero:Attack(target)
                CastIgnite(target)
                CastItems(target)
                return true
            end 


            if target.health < (qDmg + extradmg) and CastQ(target, tempballQ) then
                CastIgnite(target)
                CastItems(target)
                return true
            end

            if target.health < (qDmg + extradmg + onhitDmg) and GetDistance(target) < 500 and CastQ(target, tempballQ) then
                myHero:Attack(target)
                CastIgnite(target)
                CastItems(target)
                return true
            end

            if target.health < (qDmg + wDmg + extradmg) and ( myHero:GetSpellData(_Q).mana + myHero:GetSpellData(_W).mana)< myHero.mana and IsSpellReady(_W) and IsSpellReady(_Q) then 
                if CastW(target, tempballW) then 
                    CastIgnite(target)
                    CastItems(target)
                    return true 
                elseif CastQ(target, tempballQ) then
                    return true
                end
            end

            if target.health < (qDmg + wDmg + extradmg + onhitDmg) and GetDistance(target) < 500 and ( myHero:GetSpellData(_Q).mana + myHero:GetSpellData(_W).mana)< myHero.mana and IsSpellReady(_W) and IsSpellReady(_Q) then 
                if CastW(target, tempballW) then 
                    myHero:Attack(target)
                    CastIgnite(target)
                    CastItems(target)
                    return true 
                elseif CastQ(target, tempballQ) then
                    myHero:Attack(target)
                    return true
                end
            end


            if ConfigBasic.RCombo and target.health < (qDmg + rDmg + extradmg) and ( myHero:GetSpellData(_Q).mana + myHero:GetSpellData(_R).mana)< myHero.mana and IsSpellReady(_Q) and myHero:CanUseSpell(_R) == READY then  
                if CastR(target, tempballR) then
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastQ(target, tempballQ) then
                    return true
                end
            end

            if ConfigBasic.RCombo and target.health < (qDmg + rDmg + extradmg + onhitDmg) and  GetDistance(target) < 500 and ( myHero:GetSpellData(_Q).mana + myHero:GetSpellData(_R).mana)< myHero.mana and IsSpellReady(_Q) and myHero:CanUseSpell(_R) == READY then  
                if CastR(target, tempballR) then
                    myHero:Attack(target)
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastQ(target, tempballQ) then
                     myHero:Attack(target)
                    return true
                end
            end

            if ConfigBasic.RCombo and target.health < (wDmg + rDmg + extradmg) and ( myHero:GetSpellData(_W).mana + myHero:GetSpellData(_R).mana)< myHero.mana and IsSpellReady(_W) and myHero:CanUseSpell(_R) == READY then  
                if CastR(target, tempballR) then
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastW(target, tempballW) then 
                    CastIgnite(target)
                    CastItems(target)
                    return true
                end
            end

             if ConfigBasic.RCombo and target.health < (wDmg + rDmg + extradmg + onhitDmg) and GetDistance(target) < 500 and ( myHero:GetSpellData(_W).mana + myHero:GetSpellData(_R).mana)< myHero.mana and IsSpellReady(_W) and myHero:CanUseSpell(_R) == READY then  
                if CastR(target, tempballR) then
                    myHero:Attack(target)
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastW(target, tempballW) then 
                    myHero:Attack(target)
                    CastIgnite(target)
                    CastItems(target)
                    return true
                end
            end

             if ConfigBasic.RCombo and target.health < (qDmg + wDmg + rDmg + extradmg) and ( myHero:GetSpellData(_Q).mana + myHero:GetSpellData(_W).mana + myHero:GetSpellData(_R).mana )< myHero.mana and IsSpellReady(_Q) and IsSpellReady(_W) and myHero:CanUseSpell(_R) == READY then 
                if CastR(target, tempballR) then
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastW(target, tempballW) then
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastQ(target, tempballQ) then 
                    return true
                end
            end

            if ConfigBasic.RCombo and target.health < (qDmg + wDmg + rDmg + extradmg + onhitDmg) and GetDistance(target) < 500 and ( myHero:GetSpellData(_Q).mana + myHero:GetSpellData(_W).mana + myHero:GetSpellData(_R).mana )< myHero.mana and IsSpellReady(_Q) and IsSpellReady(_W) and myHero:CanUseSpell(_R) == READY then 
                if CastR(target, tempballR) then
                    myHero:Attack(target)
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastW(target, tempballW) then
                    myHero:Attack(target)
                    CastIgnite(target)
                    CastItems(target)
                    return true
                elseif CastQ(target, tempballQ) then 
                    myHero:Attack(target)
                    return true
                end
            end



            local ally = CanCastE(target)        
            if ally ~= nil then    

                if target.health < (eDmg + extradmg) and ( myHero:GetSpellData(_E).mana)< myHero.mana and GetDistance(ally) < eRange  then 
                    if ally ~= nil then
                        CastSpell(_E, ally)
                        CastIgnite(target)
                        CastItems(target)
                        return true
                    end
                end

                if target.health < (eDmg + wDmg + extradmg) and ( myHero:GetSpellData(_E).mana + myHero:GetSpellData(_W).mana)< myHero.mana and IsSpellReady(_E) and myHero:CanUseSpell(_W) == READY then 
                   if ally ~= nil and CastW(target) then
                        CastIgnite(target)
                        CastItems(target)
                        return true
                   end 
                end
            end

            local E = InEComboRange(target)

            if E and ConfigBasic.RCombo then
                if target.health < (eDmg + rDmg + extradmg) and ( myHero:GetSpellData(_E).mana + myHero:GetSpellData(_R).mana)< myHero.mana and IsSpellReady(_E) and myHero:CanUseSpell(_R) == READY then 
                    if CastR(target) then
                        CastIgnite(target)
                        CastItems(target)
                        return true
                    end 
                end


                if target.health < (eDmg + wDmg + rDmg + extradmg) and ( myHero:GetSpellData(_E).mana + myHero:GetSpellData(_W).mana + myHero:GetSpellData(_R).mana )< myHero.mana and IsSpellReady(_E) and IsSpellReady(_W) and  myHero:CanUseSpell(_R) == READY then 
                    if CastW(target) then
                        CastIgnite(target)
                        CastItems(target)
                        return true
                    elseif CastR(target) then
                        CastIgnite(target)
                        CastItems(target)
                        return true
                    end 
                end
            end
        end
    end

    return false
end

function IsSpellReady(Ispell)
    if myHero:CanUseSpell(_Q) == READY or (Cooldown[Ispell]-GetGameTimer() < 1.5 and Cooldown[Ispell]-GetGameTimer() > 0) then
        return true
    end 
    return false
end

function InEComboRange(target)
    for i, ally in pairs(allyHeroes) do
           if ally.valid and not ally.dead and ally.bTargetable and GetDistance(target,ally)<=rRange then
                
            local V = Vector(ally) - Vector(target)
            local k = V:normalized()
            local P = V:perpendicular2():normalized()

            local t,i,u = k:unpack()
            local x,y,z = P:unpack()


            local point1 = target.x + (x * (getHitBoxRadius(target)+35)) + (t * 825)
            local point2 = target.y + (y * (getHitBoxRadius(target)+35)) + (i * 825)
            local point3 = target.z + (z * (getHitBoxRadius(target)+35)) + (u * 825)

            local point4 = target.x + (x *(getHitBoxRadius(target)+35)) + (t * getHitBoxRadius(target))
            local point5 = target.y + (y *(getHitBoxRadius(target)+35)) + (i * getHitBoxRadius(target))
            local point6 = target.z + (z * (getHitBoxRadius(target)+35))  + (u * getHitBoxRadius(target))

            local lpoint1 = target.x - (x * (getHitBoxRadius(target)+35)) + (t * 825)
            local lpoint2 = target.y - (y * (getHitBoxRadius(target)+35))+ (i * 825)
            local lpoint3 = target.z - (z * (getHitBoxRadius(target)+35))+ (u * 825)

            local lpoint4 = target.x - (x * (getHitBoxRadius(target)+35))+ (t * getHitBoxRadius(target))
            local lpoint5 = target.y - (y * (getHitBoxRadius(target)+35))+ (i * getHitBoxRadius(target))
            local lpoint6 = target.z - (z * (getHitBoxRadius(target)+35)) + (u * getHitBoxRadius(target))

            local c = WorldToScreen(D3DXVECTOR3(point1, point2, point3))
            local l = WorldToScreen(D3DXVECTOR3(point4, point5, point6))
            local cl = WorldToScreen(D3DXVECTOR3(lpoint1, lpoint2, lpoint3))
            local ll = WorldToScreen(D3DXVECTOR3(lpoint4, lpoint5, lpoint6))

            local poly = Polygon(Point(c.x, c.y),  Point(l.x, l.y), Point(cl.x, cl.y),   Point(ll.x, ll.y))


            local b = WorldToScreen(D3DXVECTOR3(ball.x, ball.y, ball.z))
            local po = Point(b.x, b.y)
            if poly:contains(po) then
                return true
            end
        end
    end
    return false
end

function CanCastE(target)
     for i, ally in pairs(allyHeroes) do
        if ally.valid and not ally.dead and ally.bTargetable and GetDistance(target,ally)<=eRange then
                
            local V = Vector(target) - Vector(ally)
            local k = V:normalized()
            local P = V:perpendicular2():normalized()

            local t,i,u = k:unpack()
            local x,y,z = P:unpack()


            local point1 = target.x + (x * (getHitBoxRadius(target)+35)) + (t * 825)
            local point2 = target.y + (y * (getHitBoxRadius(target)+35)) + (i * 825)
            local point3 = target.z + (z * (getHitBoxRadius(target)+35)) + (u * 825)

            local point4 = target.x + (x *(getHitBoxRadius(target)+35)) + (t * (getHitBoxRadius(target) + 10))
            local point5 = target.y + (y *(getHitBoxRadius(target)+35)) + (i * (getHitBoxRadius(target) + 10))
            local point6 = target.z + (z * (getHitBoxRadius(target)+35))  + (u * (getHitBoxRadius(target) + 10))

            local lpoint1 = target.x - (x * (getHitBoxRadius(target)+35)) + (t * 825)
            local lpoint2 = target.y - (y * (getHitBoxRadius(target)+35))+ (i * 825)
            local lpoint3 = target.z - (z * (getHitBoxRadius(target)+35))+ (u * 825)

            local lpoint4 = target.x - (x * (getHitBoxRadius(target)+35))+ (t * (getHitBoxRadius(target) + 10))
            local lpoint5 = target.y - (y * (getHitBoxRadius(target)+35))+ (i * (getHitBoxRadius(target) + 10))
            local lpoint6 = target.z - (z * (getHitBoxRadius(target)+35)) + (u * (getHitBoxRadius(target) + 10))

            local c = WorldToScreen(D3DXVECTOR3(point1, point2, point3))
            local l = WorldToScreen(D3DXVECTOR3(point4, point5, point6))
            local cl = WorldToScreen(D3DXVECTOR3(lpoint1, lpoint2, lpoint3))
            local ll = WorldToScreen(D3DXVECTOR3(lpoint4, lpoint5, lpoint6))

            local poly = Polygon(Point(c.x, c.y),  Point(l.x, l.y), Point(cl.x, cl.y),   Point(ll.x, ll.y))


            local b = WorldToScreen(D3DXVECTOR3(ball.x, ball.y, ball.z))
            local po = Point(b.x, b.y)
            if poly:contains(po) then
                return ally
            end
        end
    end
    return nil

end

function CastIgnite(target)
    if ignite ~= nil and myHero:CanUseSpell(ignite) == READY and GetDistance(target) < 600 then
        CastSpell(ignite, target)
        return true
    end
    return false
end

function CastItems(target)
    if GetInventorySlotItem(DFGSlot) and myHero:CanUseSpell(DFGSlot) == READY  and GetDistance(target) < 750 then
        CastSpell(DFGSlot, target)
    end 
    if GetInventorySlotItem(HXGSlot) and myHero:CanUseSpell(HXGSlot) == READY  and GetDistance(target) < 700 then
        CastSpell(HXGSlot, target)
    end 
    if GetInventorySlotItem(BWCSlot) and myHero:CanUseSpell(BWCSlot) == READY  and GetDistance(target) < 500 then
        CastSpell(BWCSlot, target)
    end  


    if ignite ~= nil and myHero:CanUseSpell(ignite) == READY and GetDistance(target) < 600 then
        CastSpell(ignite, target)
        return true
    end
end


function Sorting()
    table.sort(enemyHeroes, function(x,y) 
        local dmgx = myHero:CalcMagicDamage(x, 100)
        local dmgy = myHero:CalcMagicDamage(y, 100)

        dmgx = dmgx/ (1 + (SortList[x.charName]/10) - 0.1)
        dmgy = dmgy/ (1 + (SortList[y.charName]/10) - 0.1)

        local valuex = x.health/dmgx
        local valuey = y.health/dmgy

        return valuex < valuey
        end)

    table.sort(allyHeroes, function(x,y) 
        local mdmgx = myHero:CalcMagicDamage(x, 100)
        local mdmgy = myHero:CalcMagicDamage(y, 100)
        local dmgx = myHero:CalcDamage(x, 100)
        local dmgy = myHero:CalcDamage(y, 100)

        dmgx = (dmgx+mdmgx)/ (1 + (SortList[x.charName]/10) - 0.1)
        dmgy = (dmgy+mdmgy)/ (1 + (SortList[y.charName]/10) - 0.1)

        local valuex = x.health/dmgx
        local valuey = y.health/dmgy

        return valuex < valuey
        end)
end

function getHitBoxRadius(target)
    return GetDistance(target, target.minBBox)/2
end


function TargetNear(object, distance, target)
    return object ~= nil and target ~= nil and object.valid and target.valid and object.networkID ~= target.networkID and not target.dead and not object.dead and target.bTargetable and object.bTargetable and GetDistance(target, object) <= distance
end

function objectValid(object)
    return object ~= nil and object.valid and not object.dead and object.bTargetable
end


function CastW(enemy, tempball)
    if not bMoving and myHero:CanUseSpell(_W) == READY and myHero:GetSpellData(_W).mana < myHero.mana and GetDistance(enemy, ball) < wRange+200 then
        local WPrediction = TargetPredictionVIP(wRange, math.huge, 0.3, nil, tempball)
        local pos, t, vec = WPrediction:GetPrediction(enemy)
        if pos ~= nil then 
            if GetDistance(pos, tempball) < wRange then
                CastSpell(_W)
                return true
            end
        end
    end 
    return false
end

function CastR(enemy, tempball)
    if not bMoving and myHero:CanUseSpell(_R) == READY and myHero:GetSpellData(_R).mana < myHero.mana and GetDistance(enemy, ball) < rRange+200 then

        local RPrediction = TargetPredictionVIP(rRange, math.huge, 0.3, nil, tempball)
        local pos, t, vec = RPrediction:GetPrediction(enemy)
        if pos ~= nil then
            if GetDistance(pos, tempball) < rRange then 
                block = false
                CastSpell(_R)
                block = true
                return true
            end
        end
            
    end
    return false
end

function CastQ(enemy, tempball)
    if not bMoving and myHero:CanUseSpell(_Q) == READY and myHero:GetSpellData(_Q).mana < myHero.mana then
        local QPrediction = TargetPredictionVIP(900, 1200, 0.250, 80, tempball)
        local pos, t, vec = QPrediction:GetPrediction(enemy)
        if pos ~= nil then
            if tempball ~= nil then
                local V = Vector(pos) - Vector(tempball)
                local p = Vector(enemy) - Vector(pos)
                local pp = p:normalized()
                local k = V:normalized()
                local t,i,u = k:unpack()
                local x,y,z = pp:unpack()
                local point1 = pos.x + (t * (getHitBoxRadius(enemy)+35)) + (x*getHitBoxRadius(enemy))
                local point2 = pos.y + (i * (getHitBoxRadius(enemy)+35)) + (y*getHitBoxRadius(enemy))
                local point3 = pos.z + (u * (getHitBoxRadius(enemy)+35)) + (z*getHitBoxRadius(enemy))
                local good = Vector(point1, point2, point3)

                if GetDistance(good) < qRange and GetDistance(good, tempball) < qRange + ballRange then
                    CastSpell(_Q, point1, point3)
                    return true
                elseif GetDistance(pos) < qRange and GetDistance(tempball, pos) < qRange + ballRange then
                    CastSpell(_Q, pos.x, pos.z)
                    return true
                end
            end
        end
    end
    return false
end

function AutoAll()
    if not bMoving then
        local QPrediction = TargetPredictionVIP(900, 1200, 0.250, 80, ball)
        local WPrediction = TargetPredictionVIP(wRange, math.huge, 0.3, nil, ball)
        local RPrediction = TargetPredictionVIP(rRange, math.huge, 0.3, nil, ball)

        local wCount = 0
        local rCount  = 0
        local rSmartCount = 0
        local fall = false

        local tempballQ = nil
        local tempballW = nil
        local tempballR = nil

        local noCast = nil

        for i, target in pairs(enemyHeroes) do
            if ValidTarget(target, qRange+ballRange) then
                 --Auto Q
                 if myHero:CanUseSpell(_Q) == READY and (ConfigBasic.Combo or ConfigBasic.Harass or (Config.AutoQ and myHero.maxMana*(Config.AutoQpMana/100) < myHero.mana and myHero.maxHealth*(Config.Autohealt/100) < myHero.health)) then
                    if tempballQ == nil then
                        if ball.type == "obj_AI_Hero" then 
                            local pos1, t1, vec1 = QPrediction:GetPrediction(ball)
                            tempballQ = pos1
                        else 
                            tempballQ = ball
                        end
                    end
                    local casted = false
                    if not selected then
                        casted = CastQ(target, tempballQ)
                    else 
                        casted = CastQ(selected, tempballQ)
                    end 
                    if not casted then 
                        if noCast == nil then
                            noCast = target 
                        end
                    else 
                        noCast = myHero
                    end

                end

                if ConfigBasic.SCombo and noCast ~= nil and not noCast.isMe and GetDistance(noCast) < GetDistance(noCast, ball) - 400 and GetDistance(ball) > 400 and ValidTarget(noCast, qRange+ballRange) then 
                    if not CastQ(myHero, tempballQ) and myHero:CanUseSpell(_E) == READY then
                        CastSpell(_E, myHero)
                    end
                end


                --Auto W 
                if myHero:CanUseSpell(_W) == READY and myHero:GetSpellData(_R).mana < myHero.mana and (ConfigBasic.Combo or ConfigBasic.Harass or (Config.AutoW and myHero.maxMana*(Config.AutoWpMana/100) < myHero.mana and myHero.maxHealth*(Config.Autohealt/100) < myHero.health)) and GetDistance(target, ball) < wRange+200 then
                    local pos, t, vec = WPrediction:GetPrediction(target)

                    if pos ~= nil then 
                        if tempballW == nil then
                            if ball.type == "obj_AI_Hero" then
                                local pos1, t1, vec1 = WPrediction:GetPrediction(ball)
                                tempballW = pos1 
                            else 
                                tempballW = ball
                            end
                        end
                        
                        if GetDistance(pos, tempballW) < wRange then
                            wCount = wCount +1
                        end
                    end
                end
                if Config.AutoWenemy <= wCount or  ((ConfigBasic.Combo or ConfigBasic.Harass) and 1 <= wCount) then
                    CastSpell(_W)
                end

                --Auto R
                if myHero:CanUseSpell(_R) == READY and myHero:GetSpellData(_R).mana < myHero.mana and (ConfigBasic.Combo or Config.AutoR ) and GetDistance(target, ball) < rRange+50 then
                    local pos, t, vec = RPrediction:GetPrediction(target)
                    
                    if pos ~= nil then
                        if tempballR == nil then
                            if ball.type == "obj_AI_Hero" then 
                                local pos1, t1, vec1 = RPrediction:GetPrediction(ball)
                                tempballR = pos1 
                            else 
                                tempballR = ball 
                            end
                        end 
                        
                        if GetDistance(pos, tempballR) < rRange then  
                            rCount = rCount + 1
                        end
                    end

                    if Config.AutoRenemy <= rCount then
                        for i, ally in pairs(allyHeroes) do
                            if objectValid(ally) and not ally.isMe and GetDistance(tempballR, ally)<=(rRange + 200) then
                                CastSpell(_R)
                                break
                            end
                        end
                    end
                end
                --Auto E
                if myHero:CanUseSpell(_E) == READY and myHero:GetSpellData(_E).mana < myHero.mana and (ConfigBasic.Combo or ConfigShield.shielddmg) then
                    local ally = CanCastE(target)
                    if objectValid(ally) and GetDistance(ally) < eRange then
                        CastSpell(_E, ally)
                    end
                end
            end
        end
    end
end